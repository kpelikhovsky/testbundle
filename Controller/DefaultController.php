<?php

namespace kpelikhovsky\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use FOS\UserBundle\Entity\UserManager;
use kpelikhovsky\TestBundle\Entity\User;
use kpelikhovsky\TestBundle\Entity\Invitation;
use kpelikhovsky\TestBundle\Entity\Referer;
use kpelikhovsky\TestBundle\Form\DataTransformer\InvitationToCodeTransformer;



class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {       
        return $this->render('TestBundle:Default:index.html.twig');
    }

    public function showCodeAction(Request $request)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager('test');

        // Set default response. Not logged user.
        $resp = new Response();
        $notLogged = $this->renderView('TestBundle:Default:invitation.html.twig', array("ref" => null));
        $resp->setContent($notLogged);

        // If user is logged in already
        if (is_object($user))
        {
            $inv = $user->getInvitation();

            // If user has invitation just show it
            if(is_object($inv))
            {
                $Transformer = new InvitationToCodeTransformer($em);
                $code = $Transformer->Transform($inv);
                $showInv = $this->renderView('TestBundle:Default:invitation.html.twig', array("ref" => $code));
                $resp->setContent($showInv);
            }
            else // Otherwise, create new
            {
                $userManager = $this->container->get('fos_user.user_manager');

                $inv = new Invitation;
                
                $inv->setEmail($user->getEmailCanonical());
                $em->persist($inv);
                
                $user->setInvitation($inv);
                $userManager->updateUser($user);
                $em->flush();

                $newInv = $this->renderView('TestBundle:Default:invitation.html.twig', array("ref" => $inv->getCode()));
                $resp->setContent($newInv);
            }
        }
        return $resp;
    }
}