<?php

namespace kpelikhovsky\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Referer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=6)
     */
    protected $ref;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $ip;
    

    /** @ORM\OneToOne(targetEntity="User", mappedBy="referer", cascade={"persist", "merge"}) */
    protected $user;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $refdate;

    /**
     * Set ip
     *
     * @param string $ip
     * @return Referal
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return Referal
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    
        return $this;
    }

    /**
     * Get ref
     *
     * @return string 
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set referer
     *
     * @param string $referer
     * @return Referal
     */
    public function setReferer($referer)
    {
        $this->referer = $referer;
    
        return $this;
    }

    /**
     * Get referer
     *
     * @return string 
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * Set refdate
     *
     * @param \DateTime $refdate
     * @return Referal
     */
    public function setRefdate($refdate)
    {
        $this->refdate = $refdate;
    
        return $this;
    }

    /**
     * Get refdate
     *
     * @return \DateTime 
     */
    public function getRefdate()
    {
        return $this->refdate;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }
}