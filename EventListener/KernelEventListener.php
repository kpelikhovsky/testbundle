<?php
namespace kpelikhovsky\TestBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;

use FOS\UserBundle\Model\UserManagerInterface;
use Doctrine\ORM\EntityManager;
use kpelikhovsky\TestBundle\Entity\User;
use kpelikhovsky\TestBundle\Entity\Invitation;
use kpelikhovsky\TestBundle\Entity\Referer;

class KernelEventListener
{
	protected $userManager;
	protected $doctrine;

	function __construct(UserManagerInterface $userManager, EntityManager $em) 
	{
        $this->userManager = $userManager;
        $this->em = $em;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) 
        {
            // don't do anything if it's not the master request
            return;
        }

        $request = $event->getRequest();

        $ref = $request->query->get('ref');

        if(is_null($ref))
        {
        	// Do nothing if no ref in URL
            return;
        }
        else
        {
        	// Redirect URL combining
        	$url = $request->getBaseUrl() . $request->getPathInfo();
	        $response = new RedirectResponse($url);

        	// Trying to find an user by invitation 
	        $refUser = $this->userManager->findUserBy(array('invitation' => $ref));

	        if(is_null($refUser))
	        {
        		// Do nothing if no user found
	        	return;
	        }
	        else
	        {
		        $TZ = new \DateTimeZone("EEST");   // Acceptable for Eastern European Summer Time (Ukraine)
		        $time = new \DateTime('now', $TZ);
		        $tstr = $time->format("Y-m-d H:i:s");

		        $refItem = new Referer();
		        $refItem->setIp($request->getClientIp());
		        $refItem->setRef($ref);
		        $refItem->setReferer($request->query->get('HTTP_REFERER'));
		        $refItem->setRefdate($time);
		        $refItem->setUser($refUser);

		        $this->em->persist($refItem);
		        $this->em->flush();

		        $response->headers->setCookie(new Cookie('ref', $ref, time() + 3600));
	        }
	        
	        $event->setResponse($response);
        }
    }
}
?>