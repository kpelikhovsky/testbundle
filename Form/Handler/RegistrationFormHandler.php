<?php

namespace kpelikhovsky\TestBundle\Form\Handler;

use FOS\UserBundle\Form\Handler\RegistrationFormHandler as BaseHandler;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegistrationFormHandler extends BaseHandler
{
    protected function onSuccess(UserInterface $user, $confirmation)
    {
        // Passing ref to user
        if($ref = $this->request->cookies->get('ref'))
        {
            $refUser = $this->userManager->findUserBy(array('invitation' => $ref));

            $TZ = new \DateTimeZone("EEST");   // Acceptable for Eastern European Summer Time (Ukraine)
        	$time = new \DateTime('now', $TZ);
        	$tstr = $time->format("Y-m-d H:i:s");

        	// Save time when user is registered with ref param
        	$user->setRefdate($time);

        	// Save user IP adress
            $user->setIp($this->request->getClientIp());

            // Save user referer
            $user->setReferer($refUser->getInvitation());

            $this->userManager->updateUser($user);

            // Clearing cookies
            $resp = new Response();
            $resp->headers->clearCookie('ref');
            $resp->send();
        }

        parent::onSuccess($user, $confirmation);
    }
}
?>