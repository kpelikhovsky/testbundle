<?php

namespace kpelikhovsky\TestBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // add your custom field
        $builder->add('firstname');
        $builder->add('lastname');

        parent::buildForm($builder, $options);
        
        $builder->remove('username');
    }

    public function getName()
    {
        return 'test_user_registration';
    }
}
?>