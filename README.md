###What is this repository for? ###

Bundle created for learn efforts. So, it probably consists some errors

* This bundle allows any registered FOS user to create 6-digits invitation code which may be added to any URL. F.e: ".../code?ref=XXXXXX"
* Any GET request with "ref" param link would be saved in database at "referer" table. 
* Any user who came with such link and got registered, will have link parent-user as "referer_id".

### How to set it up? ###

1. Make sure you have installed FOS UserBundle v 1.3.x

2. Update your app/config/config.yml with these lines:

```
#!yaml

# FOS UserBundle configuration
fos_user:
    db_driver: orm # other valid values are 'mongodb', 'couchdb' and 'propel'
    firewall_name: main
    user_class: kpelikhovsky\TestBundle\Entity\User

    registration:
        form:
            type: test_user_registration
            handler: test_user.form.handler.registration

    service:
        user_manager: test_user.test_user_manager
```